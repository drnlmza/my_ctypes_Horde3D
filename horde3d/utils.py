# *************************************************************************************************
#
# Horde3D
#   Next-Generation Graphics Engine
# --------------------------------------
# Copyright (C) 2006-2009 Nicolas Schulz
#               2008-2009 Florian Noeding (Python wrapper)
#
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
# *************************************************************************************************

import ctypes

if not 'ctypes.c_bool' in globals():
	ctypes.c_bool = ctypes.c_int


__all__ = []



try:
	h3dutils = ctypes.cdll.LoadLibrary('libHorde3DUtils.so')
except OSError:
	h3dutils = ctypes.cdll.LoadLibrary('Horde3DUtils.dll')


dumpMessages = h3dutils.h3dutDumpMessages
dumpMessages.restype = ctypes.c_bool
dumpMessages.argtypes = []
__all__.append('dumpMessages')


loadResourcesFromDisk = h3dutils.h3dutLoadResourcesFromDisk
loadResourcesFromDisk.restype = ctypes.c_bool
loadResourcesFromDisk.argtypes = [ctypes.c_char_p]
__all__.append('loadResourcesFromDisk')


_pickRay = h3dutils.h3dutPickRay
_pickRay.restype = None
_pickRay.argtypes = [
		ctypes.c_int,
		ctypes.c_float, ctypes.c_float,
		ctypes.POINTER(ctypes.c_float), ctypes.POINTER(ctypes.c_float), ctypes.POINTER(ctypes.c_float),
		ctypes.POINTER(ctypes.c_float), ctypes.POINTER(ctypes.c_float), ctypes.POINTER(ctypes.c_float),
		]
def pickRay(cameraNode, nwx, nwy):
	ox, oy, oz = ctypes.c_float(), ctypes.c_float(), ctypes.c_float()
	dx, dy, dz = ctypes.c_float(), ctypes.c_float(), ctypes.c_float()
	_pickRay(cameraNode,
			ctypes.c_float(nwx), ctypes.c_float(nwy),
			ctypes.byref(ox), ctypes.byref(oy), ctypes.byref(oz),
			ctypes.byref(dx), ctypes.byref(dy), ctypes.byref(dz),
			)
	return [[ox.value, oy.value, oz.value], [dx.value, dy.value, dz.value]]
__all__.append('pickRay')


_pickNode = h3dutils.h3dutPickNode
_pickNode.restype = ctypes.c_int
_pickNode.argtypes = [ctypes.c_int, ctypes.c_float, ctypes.c_float]
def pickNode(cameraNode, nwx, nwy):
	return _pickNode(cameraNode, ctypes.c_float(nwx), ctypes.c_float(nwy))
__all__.append('pickNode')


_showText = h3dutils.h3dutShowText
_showText.restype = None
_showText.argtypes = [ctypes.c_char_p, ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_int]
def showText(text, x, y, size, color, fontMaterialRes):
        r, g, b = color
	return _showText(text, ctypes.c_float(x), ctypes.c_float(y), ctypes.c_float(size), ctypes.c_float(r), ctypes.c_float(g), ctypes.c_float(b), fontMaterialRes)
__all__.append('showText')


_showFrameStats = h3dutils.h3dutShowFrameStats
_showFrameStats.restype = None
_showFrameStats.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int]
def showFrameStats(fontMaterialRes, boxMaterialRes, mode):
	return _showFrameStats(fontMaterialRes, boxMaterialRes, mode)
__all__.append('showFrameStats')
