# *************************************************************************************************
#
# Horde3D
#   Next-Generation Graphics Engine
# --------------------------------------
# Copyright (C) 2006-2009 Nicolas Schulz
#               2008-2009 Florian Noeding (Python wrapper)
#
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
# *************************************************************************************************

import ctypes

if not 'ctypes.c_bool' in globals():
	ctypes.c_bool = ctypes.c_int


__all__ = []



try:
	h3d = ctypes.cdll.LoadLibrary('libHorde3D.so')
except OSError:
	h3d = ctypes.cdll.LoadLibrary('Horde3D.dll')

if hasattr(h3d, 'h3dextAddTerrainNode'):
	NodeType_Terrain = 100
	__all__.append('NodeType_Terrain')

	class TerrainNodeParams(object):
		HeightTexResI = 10000
		MatResI = 10001
		MeshQualityF = 10002
		SkirtHeightF = 10003
		BlockSizeI = 10004
	__all__.append('Terrain')


	addTerrainNode = h3d.h3dextAddTerrainNode
	addTerrainNode.restype = ctypes.c_int
	addTerrainNode.argtypes = [ctypes.c_int, ctypes.c_char_p, ctypes.c_int, ctypes.c_int]
	__all__.append('addTerrainNode')

	_createTerrainGeoRes = h3d.h3dextCreateTerrainGeoRes
	_createTerrainGeoRes.restype = ctypes.c_int
	_createTerrainGeoRes.argtypes = [ctypes.c_int, ctypes.c_char_p, ctypes.c_float]
	def createTerrainGeoRes(node, resName, meshQuality):
		return _createTerrainGeoRes(node, resName, ctypes.c_float(meshQuality))
	__all__.append('createTerrainGeoRes')




